const uniqid = require('uniqid');
const moment = require('moment');
const util = require('util');
const mysql = require('mysql');

let pool;
const Qb = require('./tools/qb');

class Crud {
  constructor(item) {
    if (typeof item !== 'object') {
      throw new Error('Expected object argument on super');
    }

    const selectFields = [];

    if (item.fields && item.fields.view) {
      Object.keys(item.fields.view).forEach((field) => {
        const { table } = item;

        const {
          refColumn, refTable,
        } = item.fields.view[field];

        selectFields.push(`${refTable || table}.${refColumn || field} AS ${field}`);
      });
    }

    this.data = {
      fields: {},
      filters: {
        many: {
          query: {
            type: 'text',
          },
        },
      },
      controls: {
        many: {
          create: {
            path: `/${item.name || ''}/new`,
            class: 'btn-success',
            icon: 'plus',
            type: 'button',
            method: 'GET',
          },
        },
        rowControls: {
          edit: {
            path: `/${item.name || ''}/:id`,
            class: 'btn-warning',
            icon: 'edit',
            type: 'button',
            method: 'GET',
          },
          delete: {
            path: `/${item.name || ''}/:id`,
            class: 'btn-danger',
            icon: 'trash',
            type: 'button',
            method: 'DELETE',
          },
        },
      },
      viewFields: item.fields && item.fields.view
        ? Object.keys(item.fields.view) : (item.fields || []),
      selectFields: selectFields.join(),
    };

    this.set(item);
  }

  set(attrs) {
    Object.keys(attrs).forEach((attr) => {
      this.data[attr] = attrs[attr];
    });
  }

  create(item) {
    const { table, prefix } = this.data;

    const id = uniqid(prefix);

    return new Qb(pool).insert(table, {
      id,
      ...item,
    }).execute().then(() => id);
  }

  createMultiple(fields, rows) {
    const { table, prefix } = this.data;

    return new Qb(pool).insertMultiple(table, ['id', ...fields], rows.map(r => [uniqid(prefix), ...r])).execute();
  }

  getFields() {
    return this.data.fields;
  }

  getFilters() {
    return this.data.filters;
  }

  getControls() {
    return this.data.controls;
  }

  get(params) {
    return this.getMany(params).then(r => r[0]);
  }

  getMany(params, orderBy) {
    let whereParams = [];
    const {
      table, viewFields, selectFields, join, defaultValues, defaultOrder,
    } = this.data;

    let values = {};

    if (defaultValues) {
      values = {
        ...defaultValues,
      };
    }

    if (params) {
      values = {
        ...params,
      };
    }

    if (typeof values === 'object') {
      Object.keys(values).forEach((p) => {
        if (!values[p] || values[p] === '0' || p.endsWith('-label')) {
          return true;
        }
        if (typeof params[p] === 'object') {
          whereParams = params;
          return false;
        }

        if (p === 'query') {
          whereParams.push(viewFields.filter(f => f !== 'date' && f !== 'birthdate').map(field => ({
            field,
            value: `%${values[p]}%`,
            operation: 'LIKE',
            boolOp: 'OR',
          })));
        } else if (typeof values[p] === 'object') {
          whereParams.push({
            field: p,
            value: values[p],
            operation: 'IN',
          });
        } else if (p === 'from') {
          whereParams.push({
            field: `DATE(${table}.date)`,
            isPlainField: true,
            value: values[p] === 'today' ? moment().format('YYYY-MM-DD') : values[p],
            operation: '>=',
          });
        } else if (p === 'to') {
          whereParams.push({
            field: `DATE(${table}.date)`,
            isPlainField: true,
            value: values[p] === 'today' ? moment().format('YYYY-MM-DD') : values[p],
            operation: '<=',
          });
        } else {
          whereParams.push({
            field: `${table}.${p}`,
            value: values[p],
            operation: '=',
          });
        }

        return true;
      });
    }

    const result = new Qb(pool)
      .selectAs(table, selectFields || viewFields);

    if (join) {
      Object.keys(join).forEach((tableToJoin) => {
        result.join(tableToJoin, join[tableToJoin]);
      });
    }

    if (whereParams) {
      result.where(whereParams);
    }

    if (orderBy || defaultOrder) {
      result.orderBy(orderBy || defaultOrder);
    }

    return result.execute();
  }

  edit(fields, whereParams) {
    const { table } = this.data;

    const result = new Qb(pool)
      .update(table, fields);

    if (whereParams) {
      result.where(whereParams);
    }

    return result.execute();
  }

  delete(whereParams) {
    const { table, id } = this.data;

    const result = new Qb(pool)
      .delete(table)
      .where(whereParams || [{
        field: 'id',
        value: id,
        operation: '=',
      }])
      .execute();

    return result;
  }

  getData(attr) {
    if (typeof attr === 'object' && attr.length) {
      const fields = [];

      attr.forEach((a) => {
        fields.push(this.data[a]);
      });

      return fields;
    }

    return this.data[attr];
  }
}

module.exports = ({ credentials }) => {
  const {
    host,
    port,
    user,
    password,
    database,
    charset,
    poolSize,
    connectionLimit,
  } = credentials;

  if (typeof user === 'undefined' || typeof password === 'undefined') {
    throw new Error('`user` and `password` are required options');
  }

  pool = mysql.createPool({
    connectionLimit: connectionLimit || 100,
    pool_size: poolSize || connectionLimit || 50,
    host: host || 'localhost',
    port: port || 3306,
    user,
    password,
    database,
    charset: charset || 'utf8',
  });

  pool.query = util.promisify(pool.query);

  return Crud;
};
