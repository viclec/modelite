const mongoose = require('mongoose');
const encrypt = require('mongoose-encryption');
const autoIncrement = require('mongoose-auto-increment');

mongoose.set('useCreateIndex', true);
const { Schema } = mongoose;

const timestamps = {};

let encryptOptions;

const polygonSchema = new Schema({
  type: {
    type: String,
    enum: ['Polygon'],
    required: true,
  },
  coordinates: {
    type: [[[Number]]],
    required: true,
  },
});

function parseFields(fields) {
  const parsedFields = {};

  Object.keys(fields).forEach((field) => {
    const {
      type,
      required,
    } = fields[field];

    if (type === 'Polygon') {
      parsedFields[field] = { type: polygonSchema, required };
    } else {
      parsedFields[field] = { type, required };
    }
  });

  return parsedFields;
}

function mapID(item) {
  let parsedItem = item;

  if (typeof item === 'object' && item) {
    const { _id: id, ...rest } = item;
    parsedItem = {
      id,
      ...rest,
      creationDate: new Date(parseInt(String(id).substring(0, 8), 16) * 1000),
    };
  }
  return parsedItem;
}

function mapParamsID(params, schemaFields) {
  let parsedParams = params;
  const { ObjectId } = mongoose.Types;
  const paramsNestedIDs = params ? Object.keys(params).filter(param => param.endsWith('.id')) : [];

  if (parsedParams && typeof parsedParams === 'object') {
    Object.keys(parsedParams).forEach((field) => {
      if (parsedParams[field] && typeof parsedParams[field] === 'object') {
        if (parsedParams[field].length) {
          parsedParams[field].forEach((item, i) => {
            if (typeof item === 'object') {
              Object.keys(item).forEach((foreignField) => {
                if (foreignField === 'id' && typeof item[foreignField] === 'string') {
                  parsedParams[field][i][foreignField] = ObjectId(item[foreignField]);
                } else if (typeof item[foreignField] === 'object') {
                  Object.keys(item[foreignField]).forEach((innerForeignField) => {
                    if (innerForeignField === 'id' && typeof item[foreignField][innerForeignField] === 'string') {
                      parsedParams[field][i][foreignField][innerForeignField] = ObjectId(
                        item[foreignField][innerForeignField],
                      );
                    }
                  });
                }
              });
            }
          });
        } else {
          Object.keys(params[field]).forEach((foreignField) => {
            if (foreignField === 'id' && typeof parsedParams[field][foreignField] === 'string') {
              parsedParams[field][foreignField] = ObjectId(parsedParams[field][foreignField]);
            }
          });
        }
      } else if(parsedParams[field] === 'null') {
        parsedParams[field] = null;
      } else if (schemaFields && schemaFields[field] && typeof parsedParams[field] === 'string' && schemaFields[field].type === Number) {
        parsedParams[field] = parseFloat(parsedParams[field]);
      }
    });
  }

  if (typeof parsedParams === 'object' && parsedParams && parsedParams.id) {
    const { id, ...rest } = parsedParams;

    if (typeof id === 'object') {
      if (id.length > 0) {
        parsedParams = {
          _id: id.map(i => ObjectId(i)),
          ...rest,
        };
      } else if (id.$in) {
        parsedParams = {
          _id: {
            $in: typeof id.$in === 'object' && id.$in.length > 0
              ? id.$in.map(i => ObjectId(i)) : ObjectId(id.$in),
          },
          ...rest,
        };
      }
    } else {
      parsedParams = {
        _id: ObjectId(id),
        ...rest,
      };
    }
  }

  if (paramsNestedIDs && paramsNestedIDs.length) {
    paramsNestedIDs.forEach((param) => {
      if (typeof parsedParams[param] === 'object' && parsedParams[param].$in) {
        parsedParams[param] = {
          $in: parsedParams[param].$in.map(id => ObjectId(id)),
        };
      } else if (typeof parsedParams[param] === 'object' && parsedParams[param].length > 0) {
        parsedParams[param] = parsedParams[param].map(id => ObjectId(id));
      } else {
        parsedParams[param] = ObjectId(parsedParams[param]);
      }
    });
  }

  return parsedParams;
}

class Crud {
  constructor(item) {
    if (typeof item !== 'object') {
      throw new Error('Expected object argument on super');
    }

    const { table, fields, name } = item;

    if (!name || typeof fields !== 'object') {
      throw new Error('Options `fields` and `name` are required');
    }

    const { schema: schemaFields, view: viewFields } = fields;

    if (typeof schemaFields !== 'object') {
      throw new Error('Option `fields`.`view` is required');
    }

    this.data = {
      fields: {},
      filters: {
        many: {
          query: {
            type: 'text',
          },
        },
      },
      controls: {
        many: {
          create: {
            path: `/${name}/new`,
            class: 'btn-success',
            icon: 'plus',
            type: 'button',
            method: 'GET',
          },
        },
        rowControls: {
          edit: {
            path: `/${name}/:id`,
            class: 'btn-warning',
            icon: 'edit',
            type: 'button',
            method: 'GET',
          },
          delete: {
            path: `/${name}/:id`,
            class: 'btn-danger',
            icon: 'trash',
            type: 'button',
            method: 'DELETE',
          },
        },
      },
      viewFields: Object.keys(viewFields || schemaFields).join(' '),
    };

    this.set(item);

    const schema = new Schema(parseFields(schemaFields), {
      timestamps: {
        createdAt: timestamps.dateCreated || 'dateCreated',
        updatedAt: timestamps.dateUpdated || 'dateUpdated',
      },
    });

    schema.index({ dateCreated: 1 });
    schema.index({ dateUpdate: 1 });

    Object.keys(schemaFields).forEach((field) => {
      const { autoinc, startAt, incrementBy } = schemaFields[field];

      if (autoinc) {
        schema.plugin(autoIncrement.plugin, {
          model: table || name,
          field,
          startAt,
          incrementBy,
        });
      }
    });

    if (encryptOptions) {
      const encryptedFields = [];

      Object.keys(schemaFields).forEach((field) => {
        const { encrypt: enc } = schemaFields[field];

        if (enc) {
          encryptedFields.push(field);
        }
      });

      schema.plugin(encrypt, {
        ...encryptOptions,
        decryptPostSave: false,
        encryptedFields,
      });
    }

    this.model = mongoose.model(table || name, schema);

    return this;
  }

  set(attrs) {
    Object.keys(attrs).forEach((attr) => {
      this.data[attr] = attrs[attr];
    });
  }

  create(item) {
    const { model: Model } = this;
    const parsedItem = mapParamsID(item, this.getFields().schema);
    return Model.create(parsedItem).then(({ id }) => id);
  }

  createMultiple(items) {
    const { model: Model } = this;
    const parsedItems = items.map(item => mapParamsID(item, this.getFields().schema));
    return Model.insertMany(parsedItems).then(docs => docs.map((({ id }) => id)));
  }

  async createAndUpdateMultiple(items, fields) {
    const { model: Model } = this;
    const fieldsToCompare = typeof fields === 'object' ? fields : [fields || 'id'];
    const params = {};

    fieldsToCompare.forEach((field) => {
      if (field.includes('.')) {
        const [key1, key2] = field.split('.');
        params[field] = items.map(item => item[key1][key2]);
      } else {
        params[field] = items.map(item => item[field]);
      }
    });

    const parsedItems = items;
    const foundItems = await this
      .getMany(params);
    let itemsToInsert;

    if (foundItems && foundItems.length) {
      itemsToInsert = parsedItems.filter(parsedItem => foundItems
        .findIndex(foundItem => fieldsToCompare
          .every((field) => {
            if (field.includes('.')) {
              const [key1, key2] = field.split('.');
              return parsedItem[key1][key2].toString() === foundItem[key1][key2].toString();
            }
            return parsedItem[field] === foundItem[field];
          })) === -1);
      const itemsToUpdate = parsedItems.filter((parsedItem) => {
        const index = foundItems
          .findIndex(foundItem => fieldsToCompare
            .every((field) => {
              if (field.includes('.')) {
                const [key1, key2] = field.split('.');
                return parsedItem[key1][key2].toString() === foundItem[key1][key2].toString();
              }
              return parsedItem[field] === foundItem[field];
            }));

        if (index > -1) {
          // eslint-disable-next-line no-param-reassign
          parsedItem.id = foundItems[index].id;
          return true;
        }

        return false;
      });


      const updatePromises = itemsToUpdate.map(({ id, ...existingItem }) => this.editMany({
        id: id.toString(),
      }, existingItem));

      await Promise.all(updatePromises);
    } else {
      itemsToInsert = parsedItems;
    }

    itemsToInsert = itemsToInsert.map(item => mapParamsID(item, this.getFields().schema));

    return Model.insertMany(itemsToInsert).then(docs => docs.map((({ id }) => id)));
  }

  getFields() {
    return this.data.fields;
  }

  getFilters() {
    return this.data.filters;
  }

  getControls() {
    return this.data.controls;
  }

  get(params, fieldsToGet) {
    const { model: Model, data } = this;
    const { viewFields } = data;

    const parsedParams = mapParamsID(params);

    return Model
      .findOne(parsedParams, fieldsToGet || viewFields)
      .lean()
      .then(mapID);
  }

  getMany(params, fieldsToGet, options) {
    const { model: Model, data } = this;
    const { viewFields } = data;

    const parsedParams = mapParamsID(params);

    let results = Model
      .find(parsedParams, fieldsToGet || viewFields);

    if (typeof options === 'object') {
      const { limit, page, sortBy } = options;
      if (limit) {
        results = results.limit(limit);
      }

      if (page && limit && page > 1) {
        results = results.skip((page - 1) * limit);
      }

      if (typeof sortBy === 'object') {
        results = results
          .sort(sortBy);
      }
    }

    return results
      .lean()
      .then(docs => docs.map(mapID));
  }

  count(params) {
    const { model: Model } = this;
    const parsedParams = mapParamsID(params);
    return Model.count(parsedParams);
  }

  edit(params, fieldsToEdit) {
    const { model: Model } = this;

    const parsedParams = mapParamsID(params);
    const parsedFieldsToEdit = mapParamsID(fieldsToEdit, this.getFields().schema);

    return Model
      .updateOne(parsedParams, parsedFieldsToEdit).then((data) => Promise.resolve(data));
  }

  editMany(params, fieldsToEdit) {
    const { model: Model } = this;

    const parsedParams = mapParamsID(params);
    const parsedFieldsToEdit = mapParamsID(fieldsToEdit, this.getFields().schema);

    return Model
      .updateMany(parsedParams, parsedFieldsToEdit).then((data) => Promise.resolve(data));
  }

  delete(params) {
    const { model: Model } = this;

    const parsedParams = mapParamsID(params);

    return Model
      .deleteOne(parsedParams);
  }

  deleteMany(params) {
    const { model: Model } = this;

    const parsedParams = mapParamsID(params);

    return Model
      .deleteMany(parsedParams);
  }

  getData(attr) {
    if (typeof attr === 'object' && attr.length) {
      const fields = [];

      attr.forEach((a) => {
        fields.push(this.data[a]);
      });

      return fields;
    }

    return this.data[attr];
  }
}

module.exports = ({
  credentials, dateCreated, dateUpdated, encryption,
}) => {
  const {
    host,
    port,
    user,
    password,
    database,
    protocol,
  } = credentials;

  timestamps.dateCreated = dateCreated;
  timestamps.dateUpdated = dateUpdated;

  if (encryption) {
    const {
      encryptionKey, signingKey,
    } = encryption;

    if (!encryptionKey) {
      throw new Error('Expected parameter encryptionKey in encryption');
    }

    if (!signingKey) {
      throw new Error('Expected parameter signingKey in encryption');
    }

    encryptOptions = encryption;
  }

  let connection;

  if (typeof user === 'undefined' || typeof password === 'undefined') {
    connection =  mongoose.createConnection(`mongodb://${host || 'localhost'}:${port || 27017}/${database || ''}`, { useNewUrlParser: true, useUnifiedTopology: true })
    mongoose.connect(`mongodb://${host || 'localhost'}:${port || 27017}/${database || ''}`, { useNewUrlParser: true, useUnifiedTopology: true })
  } else if (protocol) {
    connection =  mongoose.createConnection(`${protocol || 'mongodb'}://${user}:${password}@${host || 'localhost'}/${database || ''}`, { useNewUrlParser: true, useUnifiedTopology: true })
    mongoose.connect(`${protocol || 'mongodb'}://${user}:${password}@${host || 'localhost'}/${database || ''}`, { useNewUrlParser: true, useUnifiedTopology: true })
  } else {
    connection =  mongoose.createConnection(`mongodb://${user}:${password}@${host || 'localhost'}:${port || 27017}/${database || ''}`, { useNewUrlParser: true, useUnifiedTopology: true })
    mongoose.connect(`mongodb://${user}:${password}@${host || 'localhost'}:${port || 27017}/${database || ''}`, { useNewUrlParser: true, useUnifiedTopology: true })
  }

  autoIncrement.initialize(connection);

  return Crud;
};
