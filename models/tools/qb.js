const mysql = require('mysql');

module.exports = class Query {
  constructor(pool, query, params) {
    this.query = query || '';
    this.params = params || [];
    this.pool = pool;
  }

  /* Method for executing query with parameters */
  async execute() {
    const { query, params, pool } = this;

    const result = await pool.query(query, params);

    return result;
  }

  /* Method for formating query with parameters. Useful for debugging */
  format() {
    return mysql.format(this.query, this.params);
  }

  /* Method for building query with SELECT clause */
  select(table, fieldsToSelect) {
    this.query += 'SELECT ?? FROM ?? ';
    this.params.push(fieldsToSelect, table);
    return this;
  }

  /* Method for building query with SELECT AS clause */
  selectAs(table, fieldsToSelect) {
    this.query += `SELECT ${fieldsToSelect} FROM ?? `;
    this.params.push(table);
    return this;
  }

  /* Method for creating new row */
  insert(table, rows) {
    this.query = 'INSERT INTO ?? SET ?;';
    this.params = [
      table,
      rows,
    ];
    return this;
  }

  /* Method for creating multiple rows */
  insertMultiple(table, fields, rows) {
    this.query = 'INSERT INTO ?? (??) VALUES ?';
    this.params = [
      table,
      fields,
      rows,
    ];
    return this;
  }

  /* Method for updating in place of insertion */
  onDuplicateUpdate(fields) {
    let isFirst = true;
    this.query += ' ON DUPLICATE KEY UPDATE ';
    fields.forEach((field) => {
      if (isFirst) {
        isFirst = false;
        this.query += '??=VALUES(??)';
      } else {
        this.query += ', ??=VALUES(??)';
      }
      this.params.push(field, field);
    });

    return this;
  }

  /* Method for updating in place of insertion */
  onDuplicateUpdateIf(fields, conditions) {
    let isFirst = true;
    this.query += ' ON DUPLICATE KEY UPDATE ';
    fields.forEach((field) => {
      if (isFirst) {
        isFirst = false;
        this.query += '??=IF(';
        this.params.push(field);
        const ifStatement = this.whereGroup(conditions, true);

        this.query = ifStatement.query;
        this.params = ifStatement.params;

        this.query += ', VALUES(??), ??)';
      } else {
        this.query += ',??=IF(';
        this.params.push(field);

        this.whereGroup(conditions, true);

        this.query += ', VALUES(??), ??)';
      }
      this.params.push(field, field);
    });

    return this;
  }

  /* Method for building query with UPDATE clause */
  update(table, fieldsToUpdate) {
    this.query += 'UPDATE ?? SET ?';
    this.params.push(table, fieldsToUpdate);
    return this;
  }

  /* Method for building query with UPDATE clause */
  updateSimple(table) {
    this.query += 'UPDATE ?? ';
    this.params.push(table);
    return this;
  }

  /* Method for building query with UPDATE clause */
  set(fields) {
    this.query += ' SET ?';
    this.params.push(fields);
    return this;
  }

  /* Method for building query with UPDATE clause */
  setField(field1, field2) {
    this.query += ' SET ??=??';
    this.params.push(field1, field2);
    return this;
  }

  /* Method for building query with DELETE clause */
  delete(table) {
    this.query += 'DELETE FROM ??';
    this.params.push(table);
    return this;
  }

  /* Method for building query with JOIN ON clause */
  join(tableToJoin, onClause) {
    if (typeof onClause === 'object' && onClause.length === 2) {
      this.query += ' JOIN ?? ON ?? = ??';
      this.params.push(tableToJoin, onClause[0], onClause[1]);
    } else if (typeof onClause === 'object' && onClause.length === 1) {
      this.query += ' JOIN ?? ON ';
      this.params.push(tableToJoin);
      return this.whereGroup(onClause[0], true);
    } else {
      this.query += ' JOIN ?? ';
      this.params.push(tableToJoin);
    }
    return this;
  }

  /* Method for building query with LEFT JOIN ON clause */
  joinLeft(tableToJoin, onClause) {
    if (typeof onClause === 'object' && onClause.length === 2) {
      this.query += ' LEFT JOIN ?? ON ?? = ??';
      this.params.push(tableToJoin, onClause[0], onClause[1]);
    } else if (typeof onClause === 'object' && onClause.length === 1) {
      this.query += ' LEFT JOIN ?? ON ';
      this.params.push(tableToJoin);
      return this.whereGroup(onClause[0], true);
    } else {
      this.query += ' LEFT JOIN ?? ';
      this.params.push(tableToJoin);
    }
    return this;
  }

  // TODO replace with better join()
  joinValue(tableToJoin, onClause) {
    if (typeof onClause === 'object' && onClause.length === 2) {
      this.query += ' JOIN ?? ON ?? = ?';
      this.params.push(tableToJoin, onClause[0], onClause[1]);
    } else {
      this.query += ' JOIN ?? ';
      this.params.push(tableToJoin);
    }
    return this;
  }

  /* Method for building query with ORDER BY clause */
  orderBy(fieldToSort, typeOfSort) {
    if (typeof fieldToSort === 'object') {
      const [sortBy, sortType] = fieldToSort;
      this.query += ` ORDER BY ${sortBy} ${sortType || ''}`;
    } else {
      this.query += ` ORDER BY ${fieldToSort} ${typeOfSort || ''}`;
    }
    return this;
  }

  /* Method for building query with GROUP BY clause */
  groupBy(fieldToGroup) {
    this.query += ' GROUP BY ?? ';
    this.params.push(fieldToGroup);
    return this;
  }

  /* Method for building query with GROUP BY clause */
  groupByValue(fieldToGroup) {
    this.query += ` GROUP BY ${fieldToGroup}`;
    return this;
  }

  /* Method for building query with ORDER BY clause */
  limit(limit, page) {
    if (limit && page) {
      this.query += ` LIMIT ${page},${limit}`;
    } else {
      this.query += ` LIMIT ${limit}`;
    }
    return this;
  }

  /* Method for building query with WHERE clause */
  whereJoin(fieldsToFilter) { // TODO expand where for other filters such as IN(), BETWEEN etc.
    let isFirst = true;
    fieldsToFilter.forEach((clause) => {
      if (isFirst) {
        isFirst = false;
        this.query += ` WHERE ?? ${clause.operation} (??)`;
        this.params.push(clause.field, clause.value);
      } else {
        this.query += ` ${clause.boolOp || 'AND'} ?? ${clause.operation} (??)`;
        this.params.push(clause.field, clause.value);
      }
    });
    return this;
  }

  /* Method for building query with WHERE clause */
  where(fieldsToFilter) {
    if (fieldsToFilter.length === 0) {
      return this;
    }

    this.query += ' WHERE';
    return this.whereGroup(fieldsToFilter, true);
  }

  /* Method for building query with HAVING clause */
  having(fieldsToFilter) {
    if (fieldsToFilter.length === 0) {
      return this;
    }

    this.query += ' HAVING';
    return this.whereGroup(fieldsToFilter, true);
  }

  /* Method for building query with WHERE clause */
  plain(query, parameters) {
    this.query = query;
    this.params = parameters;
    return this;
  }


  /* Method for grouping boolean clauses */
  // TODO expand where for other filters such as BETWEEN etc.
  whereGroup(fieldsToFilter, isFirstGroup, groupBoolOp) {
    if (!fieldsToFilter) {
      return this;
    }
    if (!isFirstGroup) {
      this.query += ` ${groupBoolOp || 'AND'}`;
    }

    let isFirst = true;
    let boolOperator = null;
    this.query += ' (';
    fieldsToFilter.forEach((clause, i) => {
      if (typeof clause === 'object' && clause.length > 0) {
        const result = this.whereGroup(clause, i === 0, boolOperator);
        this.query = result.query;
        this.params = result.params;
      } else {
        if (isFirst) {
          isFirst = false;
        } else {
          this.query += ` ${clause.boolOp || 'AND'}`;
        }

        if (clause.isPlainField) {
          this.query += ` ${clause.field} ${clause.operation}`;
        } else {
          this.params.push(clause.field);
          this.query += ` ?? ${clause.operation}`;
        }

        if (clause.value || (clause.value === '' && clause.enableEmpty)) {
          if (clause.dateFormat) {
            this.query += ` DATE_FORMAT(${clause.isField ? '??' : '?'}, ?)`;
            this.params.push(clause.value, clause.dateFormat);
          } else if (clause.isPlain) {
            this.query += ` (${clause.value})`;
          } else {
            this.query += ` (${clause.isField ? '??' : '?'})`;
            this.params.push(clause.value);
          }
        }
      }
      boolOperator = clause.boolOp;
    });
    this.query += ')';
    return this;
  }
};
