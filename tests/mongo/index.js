const uniqid = require('uniqid');
require('dotenv').config();

const User = require('./models/users');

function generateObjectID() {
  const timestamp = (new Date().getTime() / 1000 | 0).toString(16);
  return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, () => (Math.random() * 16 | 0).toString(16)).toLowerCase();
}

(async () => {
  const oneCreatedWithID = await User.create({
    id: generateObjectID(),
    refID: 'updatedMultiple1',
    name: 'lalala',
    position: 3,
    entity: ['feafea', 'aeaef'],
  });

  console.log({ oneCreatedWithID });
  await User.createMultiple([{
    id: generateObjectID(),
    refID: uniqid(),
    name: 'multipl1',
    position: 1,
    entity: ['feafea', 'aeaef'],
  }, {
    id: generateObjectID(),
    refID: uniqid(),
    name: 'multipl2',
    position: 2,
    entity: ['feafea', 'aeaef'],
  }]).then((multiple) => {
    console.log({ multiple });
  });
  const multipleIDs = await User.createAndUpdateMultiple([{
    refID: 'updatedMultiple1',
    name: 'updatedMultiple1',
    position: 1,
    entity: ['feafea', 'aeaef'],
  }, {
    refID: uniqid(),
    name: 'multipl2',
    position: 2,
    entity: ['feafea', 'aeaef'],
  }], 'refID');
  console.log({ multipleIDs });

  if (multipleIDs && multipleIDs.length) {
    await User.getMany({
      id: multipleIDs,
    }).then((multiple) => {
      console.log({ multiple });
    });
  }
  await User.getMany({
    id: oneCreatedWithID,
  }).then((shouldBeJustOne) => {
    console.log({ shouldBeJustOne });
  });
  const sameObjectID = '5eb6cdab1dc6421a556ba276';
  const secondParameterCreateUpdate = await User.createAndUpdateMultiple([{
    refID: 'updatedMultiple1',
    name: 'updatedMultiple1',
    position: '1',
    nestedObject: {
      id: generateObjectID(),
    },
    entity: ['feafea', 'aeaef'],
  }, {
    refID: '122',
    name: 'multipl2',
    position: 8,
    nestedObject: {
      id: sameObjectID,
      nested: {
        id: generateObjectID(),
      },
    },
    entity: ['feafea', 'aeaef'],
  }], ['refID', 'nestedObject.id']);

  if (secondParameterCreateUpdate && secondParameterCreateUpdate.length) {
    await User.getMany({
      'nestedObject.id': sameObjectID,
    }).then((multiple) => {
      console.log({ multiple: JSON.stringify(multiple) });
    });
  }

  const oneCreatedWithObjectArray = await User.create({
    id: generateObjectID(),
    name: 'objectlala',
    position: 32,
    entity: [{ lala: 'feafea' }, { lala: 'aeaef' }],
  });
  let oneCreatedWithObject = await User.create({
    id: generateObjectID(),
    name: 'nestedobjectlala',
    position: 3121,
    nestedObject: { id: generateObjectID(), lala: 'feafea', lele: ['aefea', 'feae13'] },
  });

  const oneCreatedWithPolygon = await User.create({
    id: generateObjectID(),
    name: 'nestedobjectlala',
    position: 3121,
    polygon: {
      type: 'Polygon',
      coordinates: [[
        [0, 1],
        [1, 1],
        [1, 0],
      ]],
    },
  });

  const oneFoundWithPolygon = await User.get({ id: oneCreatedWithPolygon });

  console.log({ oneFoundWithPolygon });

  await User.create({
    name: 'dummy',
    entity: ['feafea', 'aeaef'],
  }).then((oneCreated) => {
    console.log({ oneCreated });
  });

  const oneFound = await User.get();

  console.log({ oneFound });

  await User.edit({
    id: oneFound.id,
  }, {
    name: 'changed',
  });

  await User.getMany().then((many) => {
    console.log({ many });
  });

  await User.getMany({
    position: { $gte: 1, $lte: 20 },
  }).then((rangeGet) => {
    console.log({ rangeGet });
  });

  await User.getMany(null, null, {
    limit: 4,
    page: 1,
  }).then((first) => {
    console.log({ first });
  });

  await User.count().then((count) => {
    console.log({ count });
  });

  await User.getMany(null, null, {
    limit: 2,
    page: 2,
    sortBy: {
      name: 'desc',
    },
  }).then((second) => {
    console.log({ second });
  });

  await User.getMany({
    name: 'dummy',
  }).then((dummies) => {
    console.log({ dummies });
  });

  await User.get({
    id: oneCreatedWithObjectArray,
  }).then((withObjectArray) => {
    console.log({ withObjectArray });
  });

  console.log({ oneCreatedWithObject });
  oneCreatedWithObject = await User.get({
    id: oneCreatedWithObject,
  });

  console.log({ oneCreatedWithObject: oneCreatedWithObject.nestedObject });

  await User.get({
    'nestedObject.lala': oneCreatedWithObject.nestedObject.lala,
  }).then((withObject) => {
    console.log({ withObject });
  });

  await User.delete({
    id: oneFound.id,
  }).then((dummies) => {
    console.log({ dummies });
  });
})();
