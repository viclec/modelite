require('dotenv').config();

const User = require('./models/encrypt_users');

function generateObjectID() {
  // eslint-disable-next-line no-bitwise
  const timestamp = (new Date().getTime() / 1000 | 0).toString(16);
  // eslint-disable-next-line no-bitwise
  return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, () => (Math.random() * 16 | 0).toString(16)).toLowerCase();
}

(async () => {
  const oneCreatedWithID = await User.create({
    id: generateObjectID(),
    refID: 'updatedMultiple1',
    name: 'lalala',
    position: 3,
    entity: ['feafea', 'aeaef'],
  });

  User.get({
    id: oneCreatedWithID,
  }).then((user) => {
    console.log({ user });
  });
})();
