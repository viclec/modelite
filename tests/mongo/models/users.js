const Model = require('./model');

const User = new Model({
  name: 'users',
  fields: {
    schema: {
      name: {
        type: String,
      },
      entity: {
        type: Array,
      },
      refID: {
        type: String,
      },
      position: {
        type: Number,
      },
      nestedObject: {
        type: Object,
      },
      autoIncId: {
        type: Number,
        autoinc: true,
      },
      polygon: {
        type: 'Polygon',
      },
    },
  },
});

module.exports = User;
