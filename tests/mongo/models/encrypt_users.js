const Model = require('./encrypt_model');

const User = new Model({
  name: 'users',
  fields: {
    schema: {
      name: {
        type: String,
        encrypt: true,
      },
      entity: {
        type: Array,
      },
      refID: {
        type: String,
        encrypt: true,
      },
      position: {
        type: Number,
      },
      nestedObject: {
        type: Object,
      },
      polygon: {
        type: 'Polygon',
      },
    },
  },
});

module.exports = User;
