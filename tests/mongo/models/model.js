const Model = require('../../../index')({
  engine: 'MongoDB',
  credentials: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    protocol: process.env.DB_PROTOCOL,
    host: process.env.DB_HOST,
  },
});

module.exports = Model;
