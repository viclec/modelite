const Model = require('../../../index')({
  engine: 'MongoDB',
  credentials: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    protocol: process.env.DB_PROTOCOL,
    host: process.env.DB_HOST,
  },
  encryption: {
    encryptionKey: 'OLYeYIK05U76KBVO/zNbBZzk+ybloy4tQlLy2xqS37M=',
    signingKey: '9aGfaLd7ivbtQKCBJvuCqpX29tJd6PqqW4EWVD5289iunaIPkbImf/YKaPQgF/tM6ypIrsie1vf/WUZUkvtwUA==',
  },
});

module.exports = Model;
