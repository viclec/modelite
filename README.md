# Modelite

Model for MySQL and MongoDB

Engines
===
- MySQL (soon)
- [MongoDB](#mongodb)

Usage
===
Options (`Object`):
- **engine** (*required*, `Set(MongoDB)`)
- **credentials** (*required*, `Object`)

Credentials (`Object`):
- **user** (*optional*, `String`)
- **password** (*optional*, `String`)
- **database** (*optional*, `String`)
- **protocol** (*optional*, `String`, default `mongodb` for MongoDB)
- **host** (*optional*, `String`, default `localhost`)

```
const Modelite = require('modelite')({
  engine: 'MongoDB',
  credentials: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    protocol: process.env.DB_PROTOCOL,
    host: process.env.DB_HOST,
  },
});
```

Legend
===
Model
---
A `Model` is an instance of Class `modelite`.

You can pass the following options (as an Object):
- **name** (*required*, `String`)
- **fields** (*required*, `ModelFields`)
- **table** (*optional*, `String`, fallback **name**)

Example:
```
const ModelDB = require('modelite')({
  engine: 'MongoDB',
  credentials: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    protocol: process.env.DB_PROTOCOL,
    host: process.env.DB_HOST,
  },
});

const Model = new ModelDB({
  name: 'users',
  fields: {
    schema: {
      name: {
        type: String,
      },
      entity: {
        type: Array,
      },
    },
  },
});
```

Parameters
---
An object that filters `Entity`s.

Follows design on Mongoose Query Filters.

Example:
```
cosnt entity = users.get({
  id: '5cdc267dd56b5662b7b7cc0c',
  age: { $gt: '50' }
});
```

Entity
---
An `Entity` is a single item of a `Model`.

Example:
```
const ModelDB = require('./model');

const users = new ModelDB({
  name: 'users',
  fields: {
    schema: {
      name: {
        type: String,
      },
      entity: {
        type: Array,
      },
    },
  },
});

cosnt entity = users.get();
```

Field
---
An object that specifies the type of the `Model`'s field.

A field can contain:
- **type** (*required*, `Type`)
- **required** (*optional*, `Boolean`, default **false**)
- **hidden** (*optional*, `Boolean`, default **false**)

Example:
```
const field = {
    type: String,
    required: true,
};
const polygon = {
    type: 'Polygon',
};
```

ModelFields
---
An object that contains the fields of the `Model`.

Can contain:
- **schema** (*required*, `Object[Field]`)
- **view** (*optional*, `Object[Field]`, fallback **schema**)
- **add** (*optional*, `Object[Field]`)
- **edit** (*optional*, `Object[Field]`)

Example:
```
const modelFields = {
    schema: {
        name: {
            type: String,
        },
        entity: {
            type: Array,
        },
    },
    view: {
        name: {
            type: String,
        },
        entity: {
            type: Array,
        },
    },
};
```

Control
---
An object that specifies the type of the `Model`'s control.

A control can contain:
- **type** (*required*, `Set(submit,button)`)
- **method** (*optional*, `Set(GET,POST,PUT,DELETE)`)
- **action** (*optional*, `String`)
- **hidden** (*optional*, `Boolean`, default **false**)

Example:
```
const control = {
    type: 'submit',
    method: 'POST',
    action: '/profile/photos',
};
```

ModelControls
---
An object that contains the controls of the `Model`.

Can contain:
- **many** (*optional*, `Object[Control]`)
- **rowControls** (*optional*, `Object[Control]`)

Example:
```
const modelControls = {
    many: {
        new: {
            action: '/create',
        },
    },
    rowControls: {
        edit: {
            action: '/:id/',
        },
        delete: {
            method: 'DELETE',
            action: '/:id/',
        },
    },
};
```

> You can pass parameters in a action of rowControls that will be replaced afterwards (:id will be replaced with row's id, for example)

ModelFilters
---
An object that contains many `Field` to be displayed like filters on model page.

Example:
```
const modelFilters = {
    query: {
        type: String,
    },
};
```

> Default filters for a model is the above example.

<a name="mongodb"></a>
MongoDB
===

create(`Entity` item)
---
Creates a single item
> Returns `id`

createMultiple(`Array[Entity]` items)
---
Creates multiple items
> Returns `Array[id]`

createAndUpdateMultiple(`Array[Entity]` items, `String` field)
---
Creates multiple items while updating overwriting the existing ones. Optionally you can pass `field` to compare (fallback is id). For example if you want to update entities on field `referenceID`, instead of `id`.
> Returns `Array[id]`

getFields()
---
> Returns `ModelFields`

getFilters()
---
> Returns `ModelFilters`

getControls()
---
> Returns `ModelControls`

get(`Parameters` params, `String` fieldsToGet)
---
> Returns an `Entity`

fieldsToGet is an optional parameter and contains an array with fields.

Example:
```
fieldsToGet = ['name', 'email', 'date'];
```

getMany(`Parameters` params, `String` fieldsToGet, `Object` options)
---
Options can contain:
- limit `Number`
- page `Number`
- sortBy `Object`

fieldsToGet is an optional parameter and contains a string with fields separated by commas.

Example:
```
fieldsToGet = 'name, email, date';
```

sortBy is an key-value object with fields to order. Example: 
```
sortBy: {
  arg: 'asc',
  arg2: 'desc'
}
```

> Returns `Array[Entity]`

count(`Parameters` params)
---
Counts documents of model to display for pagination
> Returns `Number`

edit(`Parameters` params, `Object` fieldsToEdit)
---
Edits a single `Entity`
> Returns `Promise`

editMany(`Parameters` params, `Object` fieldsToEdit)
---
Edits multiple `Entities`
> Returns `Promise`

delete(`Parameters` params)
---
Deletes a single `Entity`
> Returns `Promise`

deleteMany(`Parameters` params)
---
Deletes multiple `Entities`
> Returns `Promise`
