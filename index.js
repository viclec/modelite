const models = require('./models');

module.exports = (options) => {
  if (!options || typeof options !== 'object') {
    throw new Error('Expected options as object');
  }
  const {
    engine,
    credentials,
    dateCreated,
    dateUpdated,
    encryption,
  } = options;

  if (!engine) {
    throw new Error('`engine` is a required option');
  }

  if (!credentials) {
    throw new Error('`credentials` is a required option');
  }

  const model = models[engine];

  if (!model) {
    throw new Error(`Unsupported engine ${engine}`);
  }

  return model({
    credentials, dateCreated, dateUpdated, encryption,
  });
};
